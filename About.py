from tkinter import *

#simple about window that includes name, product, version
class About():
	def __init__(self):
		self.aboutWdw = Toplevel()
		self.aboutWdw.title("About")
		
		bFrame = Frame(self.aboutWdw)
		bFrame.grid(column = 1, rowspan = 1)
		Label(bFrame, text = "").pack(side = LEFT)
		bFrame = Frame(self.aboutWdw)
		bFrame.grid(column = 1, rowspan = 12)
		Label(bFrame, text = "\t\t").pack(side = LEFT)
		
		abtFrame = Frame(self.aboutWdw)
		abtFrame.grid(rowspan = 3, column = 1)
		Label(abtFrame, text = "About Python Airlines").pack(side = LEFT)
		abtFrame = Frame(self.aboutWdw)
		abtFrame.grid(rowspan = 4, column = 1)
		Label(abtFrame, text = "Author: Mark Rudzinski").pack(side = LEFT)
		abtFrame = Frame(self.aboutWdw)
		abtFrame.grid(rowspan = 5, column = 1)
		Label(abtFrame, text = "Version 1.0.0").pack(side = LEFT)
		abtFrame = Frame(self.aboutWdw)
		abtFrame.grid(rowspan = 6, column = 1)
		Label(abtFrame, text = "").pack(side = LEFT)
		
		btFrame = Frame(self.aboutWdw)
		btFrame.grid(rowspan = 12, column = 1)
		Label(btFrame, text = "\t\t\t\t").pack(side = LEFT)
		btClose = Button(btFrame, text = "Close", command = self.closeAbout).pack(side = LEFT)
		Label(btFrame, text = "\t").pack(side = LEFT)
		
	def closeAbout(self):
		self.aboutWdw.destroy()