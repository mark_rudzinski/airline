#this class simply stores the data dictionary of the tables/columns in the central databasefor

class DataDictionary:
	def __init__(self, dataName):
		
		if dataName == "Aircraft":
			self.columns = {
				0: "idaircraft",
				1: "nameAircraft",
				2: "seatRows",
				3: "seatColumns",
				4: "fuelcapacity",
				5: "maxdistance",
				6: "crewmembers"
				} 
				
		elif dataName == "Destinations":
			self.columns = {
				0: "idDestination",
				1: "cityDestination",
				2: "IATAairportCode",
				3: "ICAOairportCode"
				} 
		elif dataName == "Flights":
			self.columns = {
				0: "idFlights",
				1: "idDestination",
				2: "idaircraft",
				3: "priceEconomy",
				4: "priceFirstClass",
				5: "totalCost",
				6: "distance",
				7: "flightDate",
				8: "flightDTime",
				9: "gate"
				}
		elif dataName == "Customer":
			self.columns = {
				0: "idcustomer",
				1: "firstname",
				2: "lastname",
				3: "street",
				4: "city",
				5: "state",
				6: "zip",
				7: "phone"
				}
		elif dataName == "Tickets":
			self.columns = {
				0: "idTicket",
				1: "idCustomer",
				2: "idFlight",
				3: "seatType",
				4: "seatNum",
				5: "seatPrice",
				6: "feeCost",
				7: "TotalPrice"
				}
		
	def getCol(self):
		return self.columns