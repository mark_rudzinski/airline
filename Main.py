'''
Mark Rudzinski
CSIT 216: Python
Professor T. Richards
'''

from tkinter import *
from About import *
from NewWindow import *

class Main():
	def __init__(self):
		self.mainWdw = Tk()
		self.mainWdw.title("Python Airlines")
		mainMenu = Menu(self.mainWdw)
		self.mainWdw.config(menu = mainMenu, width=600, height=342)
		
		#file menu item
		fileMenu = Menu(mainMenu, tearoff = 0)
		mainMenu.add_cascade(label = "File", menu = fileMenu)
		fileMenu.add_command(label = "About", command = self.about)
		fileMenu.add_command(label = "Exit", command = self.close)

		#destination menu item
		destinationMenu = Menu(mainMenu, tearoff = 0)
		mainMenu.add_cascade(label = "Destination", menu = destinationMenu)
		destinationMenu.add_command(label = "Edit", command = self.destinations)

		#aircraft menu item
		aircraftMenu = Menu(mainMenu, tearoff = 0)
		mainMenu.add_cascade(label = "Aircraft", menu = aircraftMenu)
		aircraftMenu.add_command(label = "Edit", command = self.aircraft)
		
		#flights menu item
		flightsMenu = Menu(mainMenu, tearoff = 0)
		mainMenu.add_cascade(label = "Flights", menu = flightsMenu)
		flightsMenu.add_command(label = "Edit", command = self.flights)
		
		#customer menu item
		customerMenu = Menu(mainMenu, tearoff = 0)
		mainMenu.add_cascade(label = "Customers", menu = customerMenu)
		customerMenu.add_command(label = "Edit", command = self.customers)
		
		#ticket menu item
		ticketsMenu = Menu(mainMenu, tearoff = 0)
		mainMenu.add_cascade(label = "Tickets", menu = ticketsMenu)
		ticketsMenu.add_command(label = "Edit", command = self.tickets)
		
		
		self.mainWdw.mainloop()

	def about(self):
		About()

	def close(self):
		self.mainWdw.destroy()

	def destinations(self):
		destinationsLabels = {
			0: "City Destination:\t\t\t",
			1: "IATA Airport Code:\t\t",
			2: "ICAO Airport Code:\t\t"
			}
		NewWindow("Destinations", destinationsLabels)
	
	def aircraft(self):
		aircraftLabels = {
			0: "Aircraft Name:\t\t\t",
			1: "Seating Rows(Max 10):\t\t",
			2: "Seating Columns(Max 42):\t\t",
			3: "Fuel Capacity:\t\t\t",
			4: "Max Distance(Km):\t\t",
			5: "Total Crew Members:\t\t"
			}
		NewWindow("Aircraft", aircraftLabels)
	
	def flights(self):
		flightslabels = {
			0: "Destination(ID):\t\t\t",
			1: "Aircraft(ID):\t\t\t",
			2: "Economy Seat Price:\t\t",
			3: "First Class Seat Price:\t\t",
			4: "Total Cost:\t\t\t",
			5: "Total Distance:\t\t\t",
			6: "Flight Date:\t\t\t",
			7: "Flight Departure Time:\t\t",
			8: "Gate Number:\t\t\t"
			}
		NewWindow("Flights", flightslabels)
		
	def customers(self):
		customerslabels = {
			0: "First Name:\t\t\t",
			1: "Last Name:\t\t\t",
			2: "Street:\t\t\t\t",
			3: "City:\t\t\t\t",
			4: "State:\t\t\t\t",
			5: "Zip:\t\t\t\t",
			6: "Phone Number:\t\t\t"
			}
		NewWindow("Customer", customerslabels)
		
	def tickets(self):
		ticketslabels = {
			0: "Customer(ID):\t\t\t",
			1: "Flight(ID):\t\t\t",
			2: "Seat Type(E/F):\t\t\t",
			3: "Available Seat :\t\t\t",
			4: "Seat Price:\t\t\t",
			5: "Fees Cost:\t\t\t",
			6: "Total Price:\t\t\t"
			}
		NewWindow("Tickets", ticketslabels)
		
Main()
input("Press Enter to close.")