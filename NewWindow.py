from tkinter import *
import tkinter.messagebox
from DataDictionary import *
from Seating import *
from RunData import *

#Creates a new pop up window for each Database.
class NewWindow:
	def __init__(self, title, labels):
		self.craftWdw = Toplevel()
		self.craftWdw.title(title + " Database")
		self.title = title
		self.entryBoxCraft = []
		self.currentRow = 0
		newID = "test1"
		totEnt = len(labels) #total number of entries(one per row)
		dataColumns = DataDictionary(self.title) #retrieves column names
		self.columnNames = dataColumns.getCol()
		buttons = {
			0: "First",
			1: "Next",
			2: "Previous", 
			3: "Search by PKey",
			4: "Add",
			5: "Delete",
			6: "Update",
			7: "Clear"
			}
		commands = {	 
			0: self.clickFirst,
			1: self.clickNext,
			2: self.clickPrevious, 
			3: self.clickSearch,
			4: self.clickAdd,
			5: self.clickDelete,
			6: self.clickUpdate,
			7: self.clickClear
			}
		
		#borders
		bFrame = Frame(self.craftWdw)
		bFrame.grid(column = 1, rowspan = 1)
		Label(bFrame, text = "").pack(side = LEFT)
		bFrame = Frame(self.craftWdw)
		bFrame.grid(column = 1, rowspan = 12)
		Label(bFrame, text = "\t\t").pack(side = LEFT)
		
		#entry labels/widgets
		for i in range (totEnt):
			frame1 = Frame(self.craftWdw)
			frame1.grid(row = i+2, column = 2, pady = 5, sticky = W)
			Label(frame1, text = labels[i]).pack(side = LEFT)
			entryB = StringVar()
			Entry(frame1, textvariable = entryB, justify = RIGHT, width = 28).pack(side = LEFT)
			self.entryBoxCraft.append(entryB)
		
		#generate primary key button
		frame1 = Frame(self.craftWdw)
		frame1.grid(row = i+3, column = 2, pady = 5, sticky = W)
		Label(frame1, text = "Primary Key:\t\t\t").pack(side = LEFT)
		self.entryKey = StringVar()
		self.entryKey.set("Leave Empty For New Add")
		Entry(frame1, textvariable = self.entryKey, justify = RIGHT, width = 28).pack(side = LEFT)
		
		#buttons
		frame1 = Frame(self.craftWdw)
		frame1.grid(row = totEnt + 3, column = 2, pady = 5, sticky = W)
		if self.title == "Tickets":
			Button(frame1, text = "Show Available Seats", command = self.showSeats).pack(side = LEFT)
			self.entryBoxCraft[2].set("Click Show Available Seats")
		else:
			Label(frame1, text = " ").pack(side = LEFT)
		frame1 = Frame(self.craftWdw)
		frame1.grid(row = totEnt + 4, column = 2, pady = 5, sticky = W)
		for i in range(4):
			button1 = Button(frame1, text = buttons[i], command = commands[i]).pack(side = LEFT)
		Label(frame1, text = "\t\t").pack(side = LEFT)	
		button1 = Button(frame1, text = buttons[7], command = commands[7]).pack(side = LEFT)
				
		#last row
		btFrame = Frame(self.craftWdw)
		btFrame.grid(row = totEnt + 5, column = 2)
		for i in range(4, 7):
			button1 = Button(btFrame, text = buttons[i], command = commands[i]).pack(side = LEFT)
		for i in range (2):
			Label(btFrame, text = "\t\t\t").pack(side = LEFT)
		Label(btFrame, text = "\t").pack(side = LEFT)
		btClose = Button(btFrame, text = "Close", command = self.close).pack(side = LEFT)
		Label(btFrame, text = "\t").pack(side = LEFT)
		
	def close(self):
		self.craftWdw.destroy()
	
	def clickAdd(self):
		try:
			queryKey = ("SELECT MAX(" + self.columnNames[0] + ") FROM " + self.title)
			newKey = RunData(queryKey, 2)
			x = newKey.getData()
			self.entryKey.set(x)
				
			columns = "("
			for i in range (len(self.columnNames)):
				columns += self.columnNames[i]
				if (i+1) != len(self.columnNames):
					columns += ", "
			columns += ") "

			data = "VALUES ('" + x + "', "
			for j in range(len(self.entryBoxCraft)):
				data += "'" + self.entryBoxCraft[j].get() + "'"
				if (j+1) != len(self.entryBoxCraft):
					data += ", "
			data += ")"
				
			insertState = "INSERT INTO " + self.title + " " + columns + data 
			RunData(insertState, 3)
		except:
			tkinter.messagebox.showerror("Error", "Data Entry Error.")

	def clickFirst(self):
		try: 
			queryStatement = self.formatQuery()
			queryStatement += " LIMIT 0,1"
			x = RunData(queryStatement, 1)
			newData = x.getData()
			for i in range(len(self.entryBoxCraft)):
				self.entryBoxCraft[i].set(newData[i+1])
			self.entryKey.set(newData[0])
			self.currentRow = 0
		except:
			for i in range(len(self.entryBoxCraft)):
				self.entryBoxCraft[i].set("Error")
			self.entryKey.set("Error")
			tkinter.messagebox.showerror("Error", "No Data in selected table.")
		
	def clickNext(self):
		try: 
			queryStatement = self.formatQuery()
			queryStatement += " LIMIT "+ str(self.currentRow + 1) + ",1"
			x = RunData(queryStatement, 1)
			newData = x.getData()
			for i in range(len(self.entryBoxCraft)):
				self.entryBoxCraft[i].set(newData[i+1])
			self.entryKey.set(newData[0])
			self.currentRow += 1
		except:
			tkinter.messagebox.showerror("Error", "No Data in next row.")
	
	def clickPrevious(self):
		try: 
			queryStatement = self.formatQuery()
			queryStatement += " LIMIT "+ str(self.currentRow - 1) + ",1"
			x = RunData(queryStatement, 1)
			newData = x.getData()
			for i in range(len(self.entryBoxCraft)):
				self.entryBoxCraft[i].set(newData[i+1])
			self.entryKey.set(newData[0])
			self.currentRow -= 1
		except:
			tkinter.messagebox.showerror("Error", "No Data in previous row.")	
	
	def clickSearch(self):
		try: 
			queryStatement = self.formatQuery()
			queryStatement += " WHERE " + self.columnNames[0] + " ='" + self.entryKey.get() + "'"
			x = RunData(queryStatement, 1)
			newData = x.getData()
			for i in range(len(self.entryBoxCraft)):
				self.entryBoxCraft[i].set(newData[i+1])
			self.currentRow = 0
		except:
			tkinter.messagebox.showerror("Error", "Invalid ID.")
	
	def clickDelete(self):
		try:
			deleteStatement = "DELETE FROM " + self.title + " WHERE " 
			deleteStatement += self.columnNames[0]  + " = '" + self.entryKey.get() + "'"
			RunData(deleteStatement, 4)
			self.clickClear()
		except:
			tkinter.messagebox.showerror("Error", "Invalid ID.")
			
	def clickUpdate(self):
		try:
			updateStatement = "UPDATE " + self.title + " SET "
			for i in range (len(self.entryBoxCraft)):
				updateStatement += self.columnNames[i + 1] + " = '" + self.entryBoxCraft[i].get() + "' "
				if (i+1) != len(self.entryBoxCraft):
					updateStatement += ", "
			updateStatement += " WHERE " + self.columnNames[0]  + " = '" + self.entryKey.get() + "'"
			RunData(updateStatement, 5)
		except:
			tkinter.messagebox.showerror("Error", "Invalid ID or Data.")
		
	def clickClear(self):
		for i in range(len(self.entryBoxCraft)):
			self.entryBoxCraft[i].set(" ")
		self.entryKey.set(" ")
		
	def formatQuery(self):
		queryStatement = "SELECT "
		for i in range (len(self.columnNames)):
				queryStatement += self.columnNames[i]
				if (i+1) != len(self.columnNames):
					queryStatement += ", "
		queryStatement += " FROM " + self.title
		return queryStatement
		
	def showSeats(self):
		try:
			queryStatement = "SELECT idaircraft FROM Flights "
			queryStatement += "WHERE idFlights = '" + self.entryBoxCraft[1].get() +"'"
			x = RunData(queryStatement, 7)
			aircraftID = x.getData()
			queryStatement = "SELECT seatRows FROM Aircraft "
			queryStatement += "WHERE idaircraft = '" + aircraftID +"'"
			y = RunData(queryStatement, 7)
			rows = y.getData()
			queryStatement = "SELECT seatColumns FROM Aircraft "
			queryStatement += "WHERE idaircraft = '" + aircraftID +"'"
			z = RunData(queryStatement, 7)
			columns = z.getData()
			Seating(int(rows), int(columns), self.entryBoxCraft[1].get())
		except:
			tkinter.messagebox.showerror("Error", "Invalid Flight ID.")

		
		