import mysql.connector

class RunData:
	def __init__(self, queryStatement, cmdType):
		cnx = mysql.connector.connect(user='Mrudzin85', password='python01',
                              host='www.doebox.com',
                              database='mrudzin85')
		print("Connected to database.")					  
		cursor = cnx.cursor()
		cursor.execute(queryStatement)
		if cmdType == 1:
			#Data Queried
			self.data = list(cursor.fetchone())
		elif cmdType == 2:
			#New Primary Key
			self.Pkey = cursor.fetchone()[0]
			keyNum = eval(self.Pkey[1:7])
			keyNum += 1
			self.data = self.Pkey[0] + str(keyNum)
		elif cmdType == 3:
			cnx.commit()
			print("Data Added.")
		elif cmdType == 4:
			cnx.commit()
			print("Data Deleted.")
		elif cmdType == 5:
			cnx.commit()
			print("Data Updated.")
		elif cmdType == 6:
			#for list of non-available seating
			self.data = [item[0] for item in cursor.fetchall()]
		elif cmdType == 7:  
			#for 1 specific data
			self.data = cursor.fetchone()[0]
		cursor.close()
		cnx.close()
		
	def getData(self):
		return self.data
	
		