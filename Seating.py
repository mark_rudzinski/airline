from tkinter import *
from RunData import *
import tkinter.messagebox

class Seating():
	def __init__(self, rows, columns, idFlight):
		self.Wdw = Toplevel()		
		self.Wdw.title("Available Seating")
		self.rowList = ["A","B","C","D","E","F","G","H","I","J"]
		
		queryStatement = "SELECT seatNum FROM tickets "
		queryStatement += "WHERE idFlight = '" + idFlight +"'"
		x = RunData(queryStatement, 6)
		self.seatTaken = x.getData()
		print(self.seatTaken)
		
		self.rows = rows
		self.columns = columns
		self.seat = ""
		
		#these are just borders
		bFrame = Frame(self.Wdw)
		bFrame.grid(column = 1, rowspan = 1)
		Label(bFrame, text = "").pack(side = LEFT)
		bFrame = Frame(self.Wdw)
		bFrame.grid(column = 1, rowspan = 12)
		Label(bFrame, text = "\t\t").pack(side = LEFT)
		
		for i in range (self.rows):
			frame = Frame(self.Wdw)
			frame.grid(row = (i +2), column = 2, padx = 1, pady = 1)
			for j in range (self.columns):
				entryBox = StringVar()
				seat = self.rowList[i] + str(j+1)
				if seat in self.seatTaken:
					entryBox.set("---")
					Entry(frame, textvariable = entryBox, justify = RIGHT, width = 4, bg = 'red').pack(side = LEFT)
				else:
					entryBox.set(seat)
					Entry(frame, textvariable = entryBox, justify = RIGHT, width = 4, bg = 'white').pack(side = LEFT)
		
		#last rows
		bFrame = Frame(self.Wdw)
		bFrame.grid(row = (i + 2), column = (j +2))
		Label(bFrame, text = "\t\t").pack(side = LEFT)
		bFrame = Frame(self.Wdw)
		bFrame.grid(row = (i + 3), column = 1)
		Label(bFrame, text = "").pack(side = LEFT)
		bFrame = Frame(self.Wdw)
		bFrame.grid(row = (i + 4), column = 1)
		Label(bFrame, text = "").pack(side = LEFT)
		bFrame = Frame(self.Wdw)
		bFrame.grid(row = (i + 5), column = 2)
		Button(bFrame, text = "Close", command = self.Wdw.destroy).pack(side = LEFT)	
		